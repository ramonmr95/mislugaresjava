package net.iescierva.ramonmr95.mislugares;

public class GeoException extends Exception {

    public GeoException(String s) {
        super(s);
    }

}
